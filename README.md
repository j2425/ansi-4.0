# Ansi-4.0

Ansible playbook:
1. **mk_instance** role creates 2 GCP instances
2. **docker_instance** role installes docker binaries inside instances
3. **gitlab_run** role:
    -   installes gitlab-runner
    -   Creates and deploys deploy_tokens in gitlab
    -   Creates projects for every GCP instance
    -   Registers runners
    -   Pushes project files into gitlab
