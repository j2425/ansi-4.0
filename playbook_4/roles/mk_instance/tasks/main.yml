---
- name: Print instance name
  debug:
    msg: "Creating instance: {{ inst_name }}"

- name: create a disk for the new instance
  google.cloud.gcp_compute_disk:
    name: "{{ inst_name }}-disk"
    size_gb: 20
    source_image: projects/debian-cloud/global/images/debian-11-bullseye-v20221102
    zone: us-west4-b
    project: "{{ gcp_project }}"
    auth_kind: "{{ gcp_cred_kind }}"
    service_account_file: "{{ gcp_cred_file }}"
    state: present
  register: inst_disk

- name: create a firewall for the instance network
  google.cloud.gcp_compute_firewall:
    name: blog-fwports
    allowed:
    - ip_protocol: tcp
      ports:
      - '8000'
    project: "{{ gcp_project }}"
    auth_kind: "{{ gcp_cred_kind }}"
    service_account_file: "{{ gcp_cred_file }}"
    state: present

- name: create an address for the new instance
  google.cloud.gcp_compute_address:
    name: "{{ inst_name }}-addr"
    region: us-west4 
    project: "{{ gcp_project }}"
    auth_kind: "{{ gcp_cred_kind }}"
    service_account_file: "{{ gcp_cred_file }}"
    state: present
  register: inst_address

- name: show inst_address.address
  debug:
    msg: "Instance {{ inst_name }} has IP - {{ inst_address.address }}"

- name: create an instance "{{ inst_name }}"
  google.cloud.gcp_compute_instance:
    name: "{{ inst_name }}"
    machine_type: e2-medium 
    disks:
    - auto_delete: 'true'
      boot: 'true'
      source: "{{ inst_disk }}"
    labels:
      environment: gitlab
      type: "{{ inst_name }}"
    network_interfaces:
    - access_configs:
      - name: External NAT
        nat_ip: "{{ inst_address }}"
        type: ONE_TO_ONE_NAT
    zone: us-west4-b
    project: "{{ gcp_project }}"
    auth_kind: "{{ gcp_cred_kind }}"
    service_account_file: "{{ gcp_cred_file }}"
    state: present

- name: get info on the firewall
  gcp_compute_firewall_info:
    filters:
    - name = blog-fwports
    project: "{{ gcp_project }}"
    auth_kind: "{{ gcp_cred_kind }}"
    service_account_file: "{{ gcp_cred_file }}"
  register: firewall_rules

- name: Refresh dynamic gcp inventory
  meta: refresh_inventory

- name: Wait for SSH instance to come up
  wait_for: host={{ inst_address.address }} port=22 delay=10 timeout=60

- name: get new ssh fingerprints
  shell: ssh-keyscan -H -trsa {{ inst_address.address }} 2>/dev/null
  changed_when: False
  register: instance_key

- name: add new instance to the known_hosts file
  ansible.builtin.known_hosts:
    name: "{{ inst_address.address }}"
    key: "{{ instance_key.stdout }}"
    state: present